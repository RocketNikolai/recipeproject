package repository

import io.reactivex.Observable
import io.reactivex.Single
import model.foodrecipes.totalnutrients.Hits


interface HitRepository {

    fun getHits(): Single<Hits>

    fun getCustomSearch(from: String, to: String, calories: String, healthlabels: List<String>, dietlabels: List<String>): Observable<Hits>

}