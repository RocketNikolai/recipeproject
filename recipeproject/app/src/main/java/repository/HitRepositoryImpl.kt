package repository

import android.util.Log
import api.ApiRecipeSearch
import api.ApiUrl
import api.ConstantsSearchRecipe
import io.reactivex.Observable
import io.reactivex.Single
import model.foodrecipes.totalnutrients.Hits
import ui.StartFragmentSearch.Companion.searchedRecipeLabel
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class HitRepositoryImpl @Inject constructor(
        private val apidatasource: ApiRecipeSearch) : HitRepository {



    override fun getHits(): Single<Hits> {
        return apidatasource.searchRecipe(q = searchedRecipeLabel, appid = ApiUrl.APP_ID, appkey = ApiUrl.APP_KEY, from = 0,
                to = 10, health = "alcohol-free", calories = "591-722")
        Log.i("TTTTTTTTTTTTTTTTTTTTTT", "sdsssssssssssssssssssssssssssssssssssss");
    }

    override fun getCustomSearch(from: String, to: String, calories: String, healthlabels: List<String>, dietlabels: List<String>): Observable<Hits> {
        return apidatasource.customSearchRecipe(q = ConstantsSearchRecipe.q, app_id = ApiUrl.APP_ID, app_key = ApiUrl.APP_KEY,
                from = from, to = to, calories = calories, healthlabels = healthlabels, dietlabels = dietlabels)
    }

}