package api

import io.reactivex.Observable
import io.reactivex.Single
import model.foodrecipes.totalnutrients.Hits

import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap


interface ApiRecipeSearch {

    @GET("/search")
    fun searchRecipe(@Query("q") q: String,
                     //@Query("r") r: String,
                     @Query("app_id") appid: String,
                     @Query("app_key") appkey: String,
                     @Query("from") from: Int,
                     @Query("to") to: Int,
                     //@Query("ingr") ingr: Int,
                     //@Query("diet") diet: String,
                     @Query("health") health: String,
                     @Query("calories") calories: String
                     //@Query("time") time: String
                     //@Query("excluded") excldued: String,
                     //@Query("callback") callback: String

    ): Single<Hits>

    @GET("/search")
    fun customSearchRecipe(@Query("q") q: String,
                           @Query("app_id") app_id: String,
                           @Query("app_key") app_key: String,
                           @Query("from") from: String,
                           @Query("to") to: String,
                           @Query("calories") calories: String,
                           @Query("dietLabels") dietlabels: List<String>,
                           @Query("healthLabels") healthlabels: List<String>
                           //@QueryMap dietandhealth: Map<String, String>
    ): Observable<Hits>
}


