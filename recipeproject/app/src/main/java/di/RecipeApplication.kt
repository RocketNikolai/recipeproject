package di

import android.app.Application



class RecipeApplication : Application(){
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    fun initializeDagger() {

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .remoteModule(RemoteModule())
                .roomModule(RoomModule())
                .firebaseModule(FirebaseModule())
                .build()
    }
}