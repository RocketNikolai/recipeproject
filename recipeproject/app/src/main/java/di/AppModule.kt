package di

import android.content.Context
import dagger.Module
import dagger.Provides
import repository.HitRepository
import ui.viewmodel.CustomViewModelFactory
import ui.viewmodel.SearchedRecipeLabelViewModelFactory
import javax.inject.Singleton

@Module
class AppModule(private val recipeApplication: RecipeApplication) {
    @Provides
    @Singleton
    fun provideContext(): Context = recipeApplication

    @Provides
    @Singleton
    fun provideViewModelFactory(hitRepository: HitRepository): SearchedRecipeLabelViewModelFactory{
        return SearchedRecipeLabelViewModelFactory(hitRepository)
    }

    @Provides
    @Singleton
    fun provideViewModelFactory2(hitRepository: HitRepository): CustomViewModelFactory{
        return CustomViewModelFactory(hitRepository)
    }
}
