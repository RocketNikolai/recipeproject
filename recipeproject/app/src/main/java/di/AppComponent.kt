package di

import dagger.Component
import repository.HitRepository
import repository.HitRepositoryImpl
import ui.*
import ui.authentification.Fragment_authentification
import ui.authentification.fragment_newuser
import ui.viewmodel.CustomViewModel
import ui.viewmodel.SearchedRecipeLabelViewModel
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RemoteModule::class, RoomModule::class, HitRepositoryModule::class, FirebaseModule::class))
@Singleton
interface AppComponent {
    fun inject(recipeSearchFragment: StartFragmentSearch)

    fun inject(recipeInfoFragment: RecipeInfoFragment)
    fun inject(customSearchFragment: CustomSearchFragment)
    fun inject(recipeFavoritesButtonFragment: FavoritesButtonFragment)
    fun inject(customSearchOutput: CustomSearchOutput)
    fun inject(testViewModel: SearchedRecipeLabelViewModel)
    fun inject(customViewModel: CustomViewModel)
    fun inject(hitRepository: HitRepository)
    fun inject(hitRepositoryImpl: HitRepositoryImpl)
    fun inject(fragment_authentification: Fragment_authentification)
    fun inject(fragment_newuser: fragment_newuser)
    fun inject(chat: Chat)
    fun inject(allUsers: AllUsers)
}