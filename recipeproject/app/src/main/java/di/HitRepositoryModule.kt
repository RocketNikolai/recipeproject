package di

import dagger.Binds
import dagger.Module
import repository.HitRepository
import repository.HitRepositoryImpl


@Module
abstract class HitRepositoryModule {
    @Binds
    abstract fun bindsHitRepository(hitRepositoryimpl: HitRepositoryImpl): HitRepository
}

