package di

import android.content.Context
import dagger.Module
import dagger.Provides
import room.RecipeDatabase
import javax.inject.Singleton

@Module
class RoomModule {
    @Provides
    @Singleton
    fun provideRoomCurrencyDataSource(context: Context) =
            RecipeDatabase.getInstance(context)
}