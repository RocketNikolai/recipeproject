package di

import api.ApiRecipeSearch
import api.ApiUrl
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton



@Module
class RemoteModule {

    @Provides
    @Singleton
    fun provideMoshi(): Moshi =
            Moshi.Builder()
                    .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build()

    @Provides
    @Singleton
    @Named("RECIPE_SEARCH_API")
    fun provideRetrofit(moshi: Moshi, okHttpClient: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(ApiUrl.API_URL)
                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build()

    @Provides
    @Singleton
    fun provideRemoteRecipeSearch(@Named("RECIPE_SEARCH_API") retrofit: Retrofit): ApiRecipeSearch =
            retrofit.create(ApiRecipeSearch::class.java)
}

