package model.foodrecipes.totalnutrients

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Ingridient(
        val text: String,
        val weight: Float
) : Parcelable