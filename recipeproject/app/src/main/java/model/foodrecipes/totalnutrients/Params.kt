package model.foodrecipes.totalnutrients

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
data class Params (
    val sane: Array<String>,
    val q: Array<String>,
    val app_key: Array<String>,
    val health: Array<String>,
    val from: Array<String>,
    val to: Array<String>,
    val calories: Array<String>,
    val app_id: Array<String>
) : Parcelable