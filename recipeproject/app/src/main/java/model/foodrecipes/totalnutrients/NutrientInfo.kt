package model.foodrecipes.totalnutrients

import android.os.Parcelable
import androidx.room.Entity
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
@Entity
data class NutrientInfo(
        val uri: String?,
        val unit: String,
        val quantity: Float,
        val label: String
) : Parcelable
