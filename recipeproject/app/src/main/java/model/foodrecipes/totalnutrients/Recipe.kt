package model.foodrecipes.totalnutrients


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Recipe(
        @PrimaryKey(autoGenerate = true)
        val recipeId: Int,
        val uri: String,
        val label: String,
        val image: String,
        val source: String,
        val url: String,
        @Json(name="yield")
        val yieldrecipe:	Int,
        var calories: Float,
        val totalWeigth: Float,
        val ingredients: Array<Ingridient>,
        val totalNutrients: TotalNutrients,
        val totalDaily: TotalNutrients
) : Parcelable





