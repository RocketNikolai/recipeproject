package model.foodrecipes.totalnutrients

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Embedded
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
@Entity
data class TotalNutrients (
        @Embedded
        val ENERC_KCAL: NutrientInfo?,
        @Embedded
        val FAT: NutrientInfo?,
        @Embedded
        val FASAT: NutrientInfo?,
        @Embedded
        val FATRN: NutrientInfo?,
        @Embedded
        val FAMS: NutrientInfo?,
        @Embedded
        val FAPU: NutrientInfo?,
        @Embedded
        val CHOCDF: NutrientInfo?,
        @Embedded
        val FIBTG: NutrientInfo?,
        @Embedded
        val SUGAR: NutrientInfo?,
        @Embedded
        val PROCNT: NutrientInfo?,
        @Embedded
        val CHOLE: NutrientInfo?,
        @Embedded
        val NA: NutrientInfo?,
        @Embedded
        val CA: NutrientInfo?,
        @Embedded
        val MG: NutrientInfo?,
        @Embedded
        val K: NutrientInfo?,
        @Embedded
        val FE: NutrientInfo?,
        @Embedded
        val ZN: NutrientInfo?,
        @Embedded
        val P: NutrientInfo?,
        @Embedded
        val VITA_RAE: NutrientInfo?,
        @Embedded
        val VITC: NutrientInfo?,
        @Embedded
        val THIA: NutrientInfo?,
        @Embedded
        val RIBF: NutrientInfo?,
        @Embedded
        val NIA: NutrientInfo?,
        @Embedded
        val VITB6A: NutrientInfo?,
        @Embedded
        val FOLDFE: NutrientInfo?,
        @Embedded
        val FOLFD: NutrientInfo?,
        @Embedded
        val VITB12: NutrientInfo?,
        @Embedded
        val VITD: NutrientInfo?,
        @Embedded
        val TOCPHA: NutrientInfo?,
        @Embedded
        val VITK1: NutrientInfo?
) : Parcelable