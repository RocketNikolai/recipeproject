package model.foodrecipes.totalnutrients
import androidx.room.Embedded
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
@Entity
data class Hit(
        @PrimaryKey(autoGenerate = true)
        val hitId: Int,
        @Embedded
        val recipe: Recipe,
        val bookmarked: Boolean,
        val bought: Boolean
) : Parcelable
