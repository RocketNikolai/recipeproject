package model.foodrecipes.totalnutrients


import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
data class Hits(
        val q: String,
        val from: Int,
        val to: Int,
        val params: Params,
        val count: Int,
        val more: Boolean,
        val hits: Array<Hit>
) : Parcelable