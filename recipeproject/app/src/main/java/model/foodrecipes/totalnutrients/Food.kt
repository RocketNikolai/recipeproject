package model.foodrecipes.totalnutrients

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
//@JsonClass(generateAdapter = true)
data class Food (
     val uri: String,
     val label: String
) : Parcelable









