package ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import repository.HitRepository
import javax.inject.Inject

class CustomViewModelFactory @Inject constructor(val hitRepository: HitRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CustomViewModel::class.java)) {
            return CustomViewModel(hitRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}