package ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import repository.HitRepository
import javax.inject.Inject

class SearchedRecipeLabelViewModelFactory @Inject constructor(val hitRepository: HitRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchedRecipeLabelViewModel::class.java)) {
            return SearchedRecipeLabelViewModel(hitRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}