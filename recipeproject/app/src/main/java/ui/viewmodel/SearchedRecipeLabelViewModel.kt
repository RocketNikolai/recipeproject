package ui.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.constraintlayout.widget.Constraints
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import api.ConstantsSearchRecipe
import di.RecipeApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import model.foodrecipes.totalnutrients.Hit
import javax.inject.Inject
import repository.HitRepository
import ui.CustomSearchFragment.Companion.calloriesrange
import ui.CustomSearchFragment.Companion.from
import ui.CustomSearchFragment.Companion.labels
import ui.CustomSearchFragment.Companion.labelsd
import ui.CustomSearchFragment.Companion.to


class SearchedRecipeLabelViewModel @Inject constructor(var hitrepository: HitRepository): ViewModel(){

    private lateinit var searchhits: MutableLiveData<Array<Hit>>
    private lateinit var customsearchrecipe: MutableLiveData<Array<Hit>>

    fun getUsers(): LiveData<Array<Hit>> {
            if (!::searchhits.isInitialized) {
                searchhits = MutableLiveData()
                loadUsers()
            }
        return searchhits
    }

    @SuppressLint("CheckResult")
    private fun loadUsers() {
            RecipeApplication.appComponent.inject(this)
             hitrepository.getHits().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Log.d("dsdsds", "$it")
                        searchhits.value = it.hits
                    }, {
                        Log.d(Constraints.TAG, "Error")
                    })
        }
}








