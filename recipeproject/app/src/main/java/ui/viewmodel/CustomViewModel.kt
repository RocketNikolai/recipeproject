package ui.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.constraintlayout.widget.Constraints
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import di.RecipeApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import model.foodrecipes.totalnutrients.Hit
import repository.HitRepository
import ui.CustomSearchFragment
import javax.inject.Inject



class CustomViewModel @Inject constructor(var hitrepository: HitRepository): ViewModel(){

    private lateinit var searchhits: MutableLiveData<Array<Hit>>
    private lateinit var customsearchrecipe: MutableLiveData<Array<Hit>>

    fun getUsers(): LiveData<Array<Hit>> {
        if (!::searchhits.isInitialized) {
            searchhits = MutableLiveData()
            loadUsers()
        }
        return searchhits
    }

    @SuppressLint("CheckResult")
    private fun loadUsers() {
        RecipeApplication.appComponent.inject(this)

        hitrepository.getCustomSearch(CustomSearchFragment.from, CustomSearchFragment.to, CustomSearchFragment.calloriesrange, CustomSearchFragment.labels!!, CustomSearchFragment.labelsd!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("dsdsds", "$it")
                    customsearchrecipe.value = it.hits
                }, {
                    Log.d(Constraints.TAG, "Error")
                })
    }
}








