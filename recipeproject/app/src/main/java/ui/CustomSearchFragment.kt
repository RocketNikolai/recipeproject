package ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.ws_035_11b.dishes.R
import kotlinx.android.synthetic.main.fragment_search.*
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.thomashaertel.widget.MultiSpinner
import di.RecipeApplication
import androidx.navigation.Navigation
import api.ConstantsSearchRecipe
import kotlinx.android.synthetic.main.content_main.*
import ui.viewmodel.SearchedRecipeLabelViewModel
import ui.viewmodel.SearchedRecipeLabelViewModelFactory
import javax.inject.Inject


class CustomSearchFragment : Fragment() {
    companion object {
        internal var labels: List<String>? = null
        internal var labelsd: List<String>? = null
        internal var from: String = "0"
        internal var to: String = "20"
        internal var calloriesrange = ConstantsSearchRecipe.calories
    }
    private var leftrange: String = "100"
    private var rightrange: String = "2000"

    @Inject
    lateinit var searchedRecipeLabelViewModelFactory: SearchedRecipeLabelViewModelFactory

    private lateinit var searchedRecipeLabelViewModel: SearchedRecipeLabelViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.let {
            it!!.bottom_nav_view.visibility = View.INVISIBLE
        }
        searchedRecipeLabelViewModel = ViewModelProviders.of(this, searchedRecipeLabelViewModelFactory).get(SearchedRecipeLabelViewModel::class.java)


        //Creating MultiSpinnners(using lib com.thomashaertel:multispinner:0.1.0@aar)
        ArrayAdapter.createFromResource(
                context,
                R.array.diet_labels,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner_dietlabels.setAdapter(adapter, false, onSelectedListenerDiet)
            val selectedItems = BooleanArray(adapter.count)
            spinner_dietlabels.selected = selectedItems
        }

        ArrayAdapter.createFromResource(
                context,
                R.array.health_labels,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner_healthlabels.setAdapter(adapter, false, onSelectedListenerHealth)
            val selectedItems = BooleanArray(adapter.count)
            spinner_healthlabels.selected = selectedItems
        }

        cutomsearch.setOnClickListener {
            from = edittext_from.text.toString()
            to = edittext_to.text.toString()
            rightrange = rightRange.text.toString()
            leftrange = leftRange.text.toString()
            calloriesrange = "$leftrange-$rightrange"
            Log.i("sddddddddddddddddddddddddddddddddd", calloriesrange)
            Navigation.findNavController(it).navigate(R.id.action_search_to_customSearchOutput)
        }
    }


    private val onSelectedListenerHealth = MultiSpinner.MultiSpinnerListener { selected ->
        // Do something here with the selected items
        labels = arrayListOf()
        for (i in selected.indices) {
            if (selected[i]) {
                (labels!! as ArrayList<String>).add(spinner_healthlabels.adapter.getItem(i).toString())

            }
        }
    }
    private val onSelectedListenerDiet = MultiSpinner.MultiSpinnerListener { selected ->
        // Do something here with the selected item
        labelsd = arrayListOf()
        for (i in selected.indices) {
            if (selected[i]) {
                Log.i("Spinner", spinner_dietlabels.adapter.getItem(i).toString())
                (labelsd!! as ArrayList<String>).add(spinner_dietlabels.adapter.getItem(i).toString())
            }
        }
    }
    //End of MultiSpinners

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }
}


