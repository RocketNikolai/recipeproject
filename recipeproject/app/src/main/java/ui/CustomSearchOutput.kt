package ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Constraints
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ws_035_11b.dishes.R
import di.RecipeApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_custom_search_output.*
import ui.adapters.CustomSearchAdapter
import ui.viewmodel.CustomViewModel
import ui.viewmodel.CustomViewModelFactory
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class CustomSearchOutput : Fragment() {

    @Inject
    lateinit var customSearchViewmodelFactory: CustomViewModelFactory

    private lateinit var customSearchViewmodel: CustomViewModel

    private lateinit var customsearchviewAdapter: CustomSearchAdapter
    private lateinit var customsearchviewManager: RecyclerView.LayoutManager

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_custom_search_output, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        customSearchViewmodel = ViewModelProviders.of(this, customSearchViewmodelFactory).get(CustomViewModel::class.java)

        customsearchviewManager = LinearLayoutManager(activity)
        customsearchviewAdapter = CustomSearchAdapter()
        customSearchOutput_recyclerview.apply {
            layoutManager = customsearchviewManager
            adapter = customsearchviewAdapter
        }

        disposable = customSearchViewmodel.hitrepository.getCustomSearch(CustomSearchFragment.from, CustomSearchFragment.to, CustomSearchFragment.calloriesrange, CustomSearchFragment.labels!!, CustomSearchFragment.labelsd!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("dsdsds", "$it")
                    customsearchviewAdapter.setHits(it.hits)
                }, {
                    Log.d(Constraints.TAG, "Error" + it.toString())
                })
    }

    override fun onDestroy() {
        if(disposable?.isDisposed == true){
            disposable?.dispose()
        }
        super.onDestroy()
    }
}
