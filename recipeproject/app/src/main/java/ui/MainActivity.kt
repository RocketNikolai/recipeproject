package ui

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import com.example.ws_035_11b.dishes.R
import com.example.ws_035_11b.dishes.R.layout.activity_main
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity(){
    val tag: String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)
        setSupportActionBar(toolbar)

        Log.i(tag, "In the onCreate() event")

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val navController = Navigation.findNavController(this, R.id.mymy_nav_host_fragment)
        val navview: NavigationView = findViewById(R.id.nav_view)
        var bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        NavigationUI.setupWithNavController(navview, navController)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(Navigation.findNavController(this, R.id.mymy_nav_host_fragment), drawer_layout)
    }

    public override fun onStart() {
        super.onStart()
        Log.i(tag, "In the onStart() event")
    }

    public override fun onRestart() {
        super.onRestart()
        Log.i(tag, "In the onRestart() event")
    }

    public override fun onResume() {
        super.onResume()
        Log.i(tag, "In the onResume() event")
    }

    public override fun onPause() {
        super.onPause()
        Log.i(tag, "In the onPause() event")
    }

    public override fun onStop() {
        super.onStop()
        Log.i(tag, "In the onStop() event")
    }

    public override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, "In the onDestroy() event")
    }
}







