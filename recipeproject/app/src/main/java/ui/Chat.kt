package ui
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import chatmodel.Chat
import chatmodel.User
import com.example.ws_035_11b.dishes.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import di.RecipeApplication
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_chat.*
import ui.adapters.ChatAdapter
import ui.authentification.Fragment_authentification
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class Chat : Fragment() {
    private lateinit var chatAdapter: ChatAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var reciever: User

    @Inject
    lateinit var emailauth: FirebaseAuth

    @Inject
    lateinit var firebaseDatabase: FirebaseDatabase

    lateinit var databaseReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
        //firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Chats")
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.bottom_nav_view.visibility = View.INVISIBLE


        reciever = (arguments!!.getParcelable("reciever") as User)


        viewManager = LinearLayoutManager(activity)
        chatAdapter = ChatAdapter()
        chat_recyclerview.apply {
            layoutManager = viewManager
            adapter = chatAdapter
        }
        var messagetext: String = ""
        sendMessage.setOnClickListener {
            messagetext = message.text.toString()
            sendMessage(messagetext, reciever.uid, emailauth.currentUser.toString())
        }
        // Read from the database

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                readMessage(reciever.uid, emailauth.currentUser.toString())
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("DatabaseError", "Failed to read value.", error.toException())
            }
        })
    }
    private fun sendMessage(message: String, reciever: String, sender: String){
        val map = hashMapOf<String, String>()
        map.put("sender", sender)
        map.put("reciever", reciever)
        map.put("message", message)
        databaseReference.push().setValue(map)
    }
    private fun readMessage(id: String, userid: String){
        var mesagechat = arrayListOf<Chat?>()
        var reference = databaseReference

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var user: Chat
                for(item in dataSnapshot.children){
                    Log.i("item count" , dataSnapshot.key)
                    val dataSnapshot = dataSnapshot.children
                    Log.i("item Key", item.getValue(Chat::class.java).toString())
                    user = item.getValue(Chat::class.java)!!
                    if(user.reciever.equals(reciever.uid)) {
                        mesagechat.add(item.getValue(Chat::class.java))
                    }
                }
                chatAdapter.setChat(mesagechat)
            }
            override fun onCancelled(databaseError: DatabaseError) {
                println("The read failed: " + databaseError.code)
            }
        })
    }
}
