package ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ws_035_11b.dishes.R
import di.RecipeApplication
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_custom_search_output.*
import kotlinx.android.synthetic.main.recipe_info_fragment.*
import model.foodrecipes.totalnutrients.Hit
import room.RecipeDatabase
import ui.adapters.CustomSearchAdapter
import ui.adapters.NutrientsAdapter
import javax.inject.Inject

class RecipeInfoFragment : Fragment() {

    @Inject
    lateinit  var db: RecipeDatabase

    var pressedhit: Hit? = null

    private lateinit var nutrientsAdapter: NutrientsAdapter
    private lateinit var nutrientsManager: RecyclerView.LayoutManager


    public var TAG: String = "InfoFragment"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "onCreate")
        RecipeApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView")
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.recipe_info_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        savedb.setOnClickListener {
            Single.fromCallable {db?.recipeDao()?.insertAll(pressedhit!!)}.subscribeOn(Schedulers.io())
                    .subscribe({
                        Log.i("INSERTED ", "$it")
                    }, {
                        Log.i("Error ", "$it")
                    })
        }

        pressedhit = arguments!!.getParcelable("SendHits") as Hit
        Log.i("ssds", pressedhit!!.recipe.label.toString())
        //set image and views
        Glide.with(imageView3.context)
                .load(pressedhit!!.recipe.image)
                .into(imageView3)
        ihyper.text = pressedhit!!.recipe.url
        customsearch_.text = pressedhit!!.recipe.calories.toString()
        nutrientsManager = LinearLayoutManager(activity)
        nutrientsAdapter = NutrientsAdapter()
        irecycler.apply {
            layoutManager = nutrientsManager
            adapter = nutrientsAdapter
        }

        nutrientsAdapter.setHits(pressedhit!!)
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "FragmentInfo onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "FragmentInfo onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "FragmentInfo onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "FragmentInfo onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i(TAG, "Fragmentinfo onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "FragmentInfo onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.i(TAG, "FragmentInfo onDetach")
    }
}


