package ui


import android.content.ClipData
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import chatmodel.Chat
import chatmodel.User
import com.example.ws_035_11b.dishes.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import di.RecipeApplication
import kotlinx.android.synthetic.main.fragment_all_users.*
import kotlinx.android.synthetic.main.fragment_blank.*
import ui.adapters.AllUsersAdapter
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class AllUsers : Fragment() {

    private lateinit var viewAdapter: AllUsersAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var dbref: DatabaseReference
    @Inject
    lateinit var fbdb: FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
        dbref = fbdb.getReference("Users")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        viewManager = LinearLayoutManager(activity)
        viewAdapter = AllUsersAdapter()
        allUsers.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        ReadAllUsers(dbref)
    }

    private fun ReadAllUsers(reference: DatabaseReference){
        val userslist = arrayListOf<User?>()
        reference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, error.code.toString(), Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for(item in dataSnapshot.children){
                    Log.i("item count" , dataSnapshot.children.toString())
                    val dataSnapshot = dataSnapshot.children
                    userslist.add(item.getValue(User::class.java))
                }
                viewAdapter.setUsers(userslist)
            }

        })
    }
}
