package ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.constraintlayout.widget.Constraints
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ws_035_11b.dishes.R
import di.RecipeApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_blank.*
import ui.adapters.RecipeLabelAdapter
import androidx.lifecycle.ViewModelProviders
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import model.foodrecipes.totalnutrients.Hit
import ui.viewmodel.SearchedRecipeLabelViewModel
import ui.viewmodel.SearchedRecipeLabelViewModelFactory
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [StartFragmentSearch.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [StartFragmentSearch.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

class StartFragmentSearch : Fragment() {


    @Inject
    lateinit var searchedRecipeLabelViewModelFactory: SearchedRecipeLabelViewModelFactory

    private lateinit var searchedRecipeLabelViewModel: SearchedRecipeLabelViewModel

    val TAG: String = "RecipeError"
    val LOG_TAG: String = "TestLifecycle"

    private lateinit var viewAdapter: RecipeLabelAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var disposable: Disposable? = null

    companion object {
        var searchedRecipeLabel = ""
        var counter = 0
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        Log.i(LOG_TAG, "Fragment1 onAttach")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
        Log.i(LOG_TAG, "Fragment1 onCreate")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i(LOG_TAG, "Fragment1 onActivityCreted")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d(LOG_TAG, "Fragment1 onCreateView")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewManager = LinearLayoutManager(activity)
        viewAdapter = RecipeLabelAdapter()
        recipe_recyclerview.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

        activity.let {
            it!!.bottom_nav_view.visibility = View.VISIBLE
            it!!.toolbar.visibility = View.VISIBLE
        }
                Log.i("serchedRecipelabel", searchedRecipeLabel.length.toString())

            searchedRecipeLabelViewModel = ViewModelProviders.of(this, searchedRecipeLabelViewModelFactory).get(SearchedRecipeLabelViewModel::class.java)
            if(!searchedRecipeLabel.equals("")){
                startfragmentsearch_progressBar.visibility = View.VISIBLE
                searchedRecipeLabelViewModel = ViewModelProviders.of(this, searchedRecipeLabelViewModelFactory).get(SearchedRecipeLabelViewModel::class.java)
                searchedRecipeLabelViewModel.getUsers().observe(this, Observer<Array<Hit>>{ searchits ->
                viewAdapter.setHits(searchits)
                    startfragmentsearch_progressBar.visibility = View.GONE
            })}

        Log.i(LOG_TAG, "Fragment1 onViewCreated")
        counter++
        q_search_food_name.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                Log.i("QueryTextSubmit", "aaaaaaaa")
                startfragmentsearch_progressBar.visibility = View.VISIBLE
                q_search_food_name.clearFocus()
                searchedRecipeLabel = q_search_food_name.getQuery().toString()
                disposable = searchedRecipeLabelViewModel.hitrepository.getHits()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Log.d("dsdsds", "$it")
                            viewAdapter.setHits(it.hits)
                            startfragmentsearch_progressBar.visibility = View.GONE
                        }, {
                            Log.d(Constraints.TAG, "Error" + it.message)
                        })
                return true
            }
            override fun onQueryTextChange(p0: String?): Boolean {
                Log.i("QueryTextChanged", "bbbbbbbbbbb")
                return true
            }
        })
    }

    override fun onStart() {
        super.onStart()
        Log.i(LOG_TAG, "Fragment1 onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i(LOG_TAG, "Fragment1 onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(LOG_TAG, "Fragment1 onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(LOG_TAG, "Fragment1 onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(LOG_TAG, "Fragment1 onDestroyView")
    }

    override fun onDestroy() {
        if(searchedRecipeLabel != "" && disposable?.isDisposed == true){
            disposable?.dispose()
        }
        super.onDestroy()
        Log.d(LOG_TAG, "Fragment1 onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(LOG_TAG, "Fragment1 onDetach")
    }
}

