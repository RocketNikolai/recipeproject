package ui.authentification


import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import chatmodel.User
import com.example.ws_035_11b.dishes.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import di.RecipeApplication
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_newuser.*
import javax.inject.Inject


class fragment_newuser : Fragment() {

    lateinit var email: String
    lateinit var password: String
    private var users: User? = null

    @Inject
    lateinit var emailauth: FirebaseAuth
    @Inject
    lateinit var firebaseDatabase: FirebaseDatabase

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.ws_035_11b.dishes.R.layout.fragment_newuser, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.bottom_nav_view.visibility = View.INVISIBLE

        button_compplete.setOnClickListener {
            email = registration_email.text.toString()
            password = newuser_edittext_checkpassword.text.toString()
            if(email != "" && password != "" && edittext_name.text.toString() != "" && edittext_surname.text.toString() != "") {
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "fill email", Toast.LENGTH_SHORT).show()
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(context, "fill password", Toast.LENGTH_SHORT).show()
                }
                if (password.length < 6) {
                    Toast.makeText(context, "password length more then 6", Toast.LENGTH_SHORT).show()
                }
                activity?.let { it1 ->
                    emailauth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(it1) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("sss", "createUserWithEmail:success")
                            val user = emailauth.currentUser
                            firebaseDatabase.getReference("Users").child(user!!.uid)
                            users = User(user.uid, edittext_name.text.toString(), edittext_surname.toString(), "Zaglushka")
                            firebaseDatabase.getReference("Users").push().setValue(users)
                            Navigation.findNavController(it).navigate(R.id.action_fragment_newuser_to_fragment_authentification)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("sss", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(context, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

}
