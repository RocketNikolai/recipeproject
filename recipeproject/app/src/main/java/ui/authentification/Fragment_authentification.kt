package ui.authentification

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.navigation.Navigation
import com.example.ws_035_11b.dishes.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_authentification.*
import com.google.firebase.auth.AuthResult
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import di.RecipeApplication
import javax.inject.Inject


class Fragment_authentification : Fragment() {


        @Inject
        lateinit var emailauth: FirebaseAuth
        @Inject
        lateinit var firebaseDatabase: FirebaseDatabase

        lateinit var databaseReference: DatabaseReference

    private var checklogin: SharedPreferences? = null
    private var checkpassword: SharedPreferences? = null
    private var email = ""
    private var password = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        button_sign_in.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.fragment_newuser))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RecipeApplication.appComponent.inject(this)
        //firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Chats")
    }

    override fun onStart() {
        super.onStart()
        if(checklogin != null && checkpassword != null) {
            val login = checklogin!!.getString("login", "")
            val pass = checkpassword!!.getString("login", "")
            if ((login != "" && login != null) && (pass != "" && pass != null)) {
                /*Navigation.findNavController(this.activity!!, R.id.mymy_nav_host_fragment)
                        .navigate(R.id.action_fragment_authentification_to_startFragment)*/
            }
        }
      }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.ws_035_11b.dishes.R.layout.fragment_authentification, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        button_sign_in.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.fragment_newuser))

        button_log_in.setOnClickListener {
            email = edittext_email.text.toString()
            password = edittext_password.text.toString()
            if (email != "" && password != "") {
                activity?.let { it1 ->
                    emailauth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(it1, OnCompleteListener<AuthResult> { task ->
                                if (task.isSuccessful) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "signInWithEmail:success")
                                    checklogin = activity?.getPreferences(Context.MODE_PRIVATE) ?: return@OnCompleteListener
                                    with (checklogin!!.edit()) {
                                        putString("login", email)
                                        commit()
                                    }
                                    checkpassword = activity?.getPreferences(Context.MODE_PRIVATE) ?: return@OnCompleteListener
                                    with (checkpassword!!.edit()) {
                                        putString("password", password)
                                        commit()
                                    }

                                    Navigation.findNavController(it).navigate(R.id.action_fragment_authentification_to_startFragment)
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                                    Toast.makeText(context, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show()
                                }

                                // ...
                            })
                    }
             }else{
                Toast.makeText(context, "fill all fields",
                        Toast.LENGTH_SHORT).show()
            }
         }

    }
}

