package ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ws_035_11b.dishes.R
import model.foodrecipes.totalnutrients.Hit

class NutrientsAdapter() : RecyclerView.Adapter<NutrientsAdapter.ViewHolder>() {

    var listhits: Hit? = null

    class ViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        var text: TextView
        var weight: TextView
        init {
            text = itemview.findViewById(R.id.ntext)
            weight = itemview.findViewById(R.id.nweight)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NutrientsAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.info_items, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - replace the contents of the view with that element
        val itemposition = listhits!!.recipe.ingredients[position]
        //weight
        holder.text.text = itemposition!!.text
        holder.weight.text = itemposition!!.weight.toString()
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        val alient = listhits!!.recipe.ingredients.size ?: 0
        return  alient
    }

    fun setHits(arrlisthits: Hit?) {
        this.listhits = arrlisthits
        notifyDataSetChanged()
    }
}












