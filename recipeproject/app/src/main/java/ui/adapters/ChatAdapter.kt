package ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import chatmodel.Chat
import com.example.ws_035_11b.dishes.R



class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    var listchat: ArrayList<Chat?>? = null

    class ViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        var chatmessage: TextView
        var chatimage: ImageView
        init {
            chatmessage = itemview.findViewById(R.id.chatmessage)
            chatimage = itemview.findViewById(R.id.profileImage)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.chat_item_right, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - replace the contents of the view with that element
        val itemposition = listchat!![position]
        holder.chatmessage.text = itemposition!!.message
        //image glide
        /*Glide.with(holder.chatimage.context)
                .load(itemposition.image)
                .into(holder.recipeimage);*/
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        val alient = listchat?.size ?: 0
        return alient
    }

    fun setChat(arrlistchat: ArrayList<Chat?>) {
        this.listchat = arrlistchat
        for(item in arrlistchat){
            if(item!!.message.equals("cheburek")){
                arrlistchat.remove(item)
            }
        }
        notifyDataSetChanged()
    }
}