package ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import chatmodel.User
import androidx.navigation.Navigation.findNavController
import com.example.ws_035_11b.dishes.R


class AllUsersAdapter() : RecyclerView.Adapter<AllUsersAdapter.ViewHolder>() {

    var listuser: ArrayList<User?>? = null

    class ViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        var username: TextView
        var image: ImageView
        init {
            username = itemview.findViewById(R.id.user_name)
            image = itemview.findViewById(R.id.user_imageurl)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllUsersAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.allusers_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - replace the contents of the view with that element
        val itemposition = listuser!![position]
        holder.username.text = itemposition!!.name

        holder.itemview.setOnClickListener{
                var recieveruser = bundleOf("reciever" to itemposition)
        findNavController(holder.itemview).navigate(R.id.action_allUsers_to_chat, recieveruser)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        val alient = listuser?.size ?: 0
        return alient
    }

    fun setUsers(arrlistchat: ArrayList<User?>) {
        this.listuser = arrlistchat
        notifyDataSetChanged()
    }
}