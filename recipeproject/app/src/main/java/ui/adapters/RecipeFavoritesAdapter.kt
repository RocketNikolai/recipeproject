package ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ws_035_11b.dishes.R
import com.example.ws_035_11b.dishes.databinding.FragmentFavoritesCardviewBinding
import model.foodrecipes.totalnutrients.Hit
import model.foodrecipes.totalnutrients.Recipe

class RecipeFavoritesAdapter() : RecyclerView.Adapter<RecipeFavoritesAdapter.FavoritesViewHolder>() {

    var listfavoriteshits: List<Hit>? = null

    class FavoritesViewHolder(var itemview: FragmentFavoritesCardviewBinding) : RecyclerView.ViewHolder(itemview.root) {
        fun bind(item: Recipe) {
            itemview.apply {
                recipe = item
                executePendingBindings()
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeFavoritesAdapter.FavoritesViewHolder{
        return FavoritesViewHolder(FragmentFavoritesCardviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        // - replace the contents of the view with that element
        val itemposition = listfavoriteshits!!.get(position)

        holder.apply {
            bind(itemposition.recipe)
            itemView.tag = itemposition.recipe.calories
        }

        holder.itemView.setOnClickListener {
            var bundle = bundleOf("SendHits" to itemposition)
            Navigation.findNavController(holder.itemView).navigate(R.id.action_favoritesButtonFragment_to_recipeInfoFragment, bundle)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        val count = listfavoriteshits?.size ?: 0
        return count
    }
    //Setter for listfavoriteshits
    fun setHits(arrlisthits: List<Hit>) {
        this.listfavoriteshits = arrlisthits
        notifyDataSetChanged()
    }
}