package ui.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import com.bumptech.glide.Glide
import com.example.ws_035_11b.dishes.R
import model.foodrecipes.totalnutrients.Hit

class RecipeLabelAdapter() : RecyclerView.Adapter<RecipeLabelAdapter.ViewHolder>() {

    var listhits: Array<Hit>? = null

    class ViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        var recipelabel: TextView
        var recipeimage: ImageView
        init {
            recipelabel = itemview.findViewById(R.id.textView_recipe_text_label)
            recipeimage = itemview.findViewById(R.id.recipe_image)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeLabelAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recipe_food_label, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - replace the contents of the view with that element
        val itemposition = listhits!!.get(position)
        //weight
        holder.recipelabel.text = itemposition.recipe.label
        //image glide
        Glide.with(holder.recipeimage.context)
                .load(itemposition.recipe.image)
                .into(holder.recipeimage);

        //view clickListener
        holder.itemview.setOnClickListener {
            var bundle = bundleOf("SendHits" to itemposition)
            findNavController(holder.itemview).navigate(R.id.action_blankFragment_to_recipeInfoFragment2, bundle)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        val alient = listhits?.size ?: 0
        return alient
    }

    fun setHits(arrlisthits: Array<Hit>) {
        this.listhits = arrlisthits
        notifyDataSetChanged()
    }
}












