package ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ws_035_11b.dishes.R
import di.RecipeApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import model.foodrecipes.totalnutrients.Hit
import room.RecipeDatabase
import ui.adapters.RecipeFavoritesAdapter
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FavoritesButtonFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class FavoritesButtonFragment : Fragment() {
    @Inject
    lateinit var dbroom: RecipeDatabase

    private lateinit var viewAdapter: RecipeFavoritesAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var disposable: Disposable

    private val fragment_favorites_recyclerview by lazy { view!!.findViewById<RecyclerView>(R.id.recipe_fragment_favorites_recyclerview) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("Favorites", "onCreate")
        RecipeApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i("Favorites", "onCreateView")
            // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites_recyclerview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("Favorites", "onViewCreated")
        viewAdapter = RecipeFavoritesAdapter()
        viewManager = LinearLayoutManager(activity)

        fragment_favorites_recyclerview.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        //select all query into db and setting recyclerview adapter
        disposable = dbroom?.recipeDao()?.getAllrecipes()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    Log.d("SendFavorites", "$it")
                    viewAdapter.setHits(it as List<Hit>)
                },{
                    Log.d("ErrorFavorites", "$it")
                })!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
       Log.i("Favorites","OonActivityCreated")
    }

    override fun onStart() {
        super.onStart()
        Log.i("Favorites", "Favorites onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("Favorites", "Favorites onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("Favorites", "Favorites onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("Favorites", "Favorites onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i("Favorites", "Favorites onDestroyView")
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
        Log.i("Favorites", "Favorites onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.i("Favorites", "Favorites onDetach")
    }
}



