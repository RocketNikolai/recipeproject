package room


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import model.foodrecipes.totalnutrients.Hit
import androidx.room.OnConflictStrategy
import io.reactivex.Flowable


@Dao
interface RecipeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(hit: Hit)

    @Query("SELECT * FROM Hit")
    fun getAllrecipes(): Flowable<List<Hit>>

    /*
    @Query("Delete from Hit order by hitId desc limit 1")
    fun deletelastrecord(): Flowable<List<Hit>>
    */
}




