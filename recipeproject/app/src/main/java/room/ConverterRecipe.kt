package room

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import model.foodrecipes.totalnutrients.Ingridient
import model.foodrecipes.totalnutrients.TotalNutrients


class ConverterRecipe {
    var moshi: Moshi  = Moshi.Builder().build()
    @TypeConverter
    fun fromTimestamp(dietLabels: Array<String>): String? {
        return if (dietLabels == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(Array<String>::class.java)
            val json: String = jsonAdapter.toJson(dietLabels)
            return json
        }
    }

    @TypeConverter
    fun dateToTimestamp(arrayString: String): Array<String>? {
        return if (arrayString == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(Array<String>::class.java)
            val arrString: Array<String> = jsonAdapter.fromJson(arrayString)!!
            return arrString
        }
    }

    @TypeConverter
    fun fromIngrientArray(arrayString: String): Array<Ingridient>? {
        return if (arrayString == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(Array<Ingridient>::class.java)
            val arrString: Array<Ingridient> = jsonAdapter.fromJson(arrayString)!!
            return arrString
        }
    }
    @TypeConverter
    fun toIngridientArray(ingridient: Array<Ingridient>): String? {
        return if (ingridient == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(Array<Ingridient>::class.java)
            val json: String = jsonAdapter.toJson(ingridient)
            return json
        }
    }

    @TypeConverter
    fun fromTotalNutrients(arrayString: String): TotalNutrients? {
        return if (arrayString == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(TotalNutrients::class.java)
            val arrString: TotalNutrients = jsonAdapter.fromJson(arrayString)!!
            return arrString
        }
    }
    @TypeConverter
    fun toTotalNutrientsString(ingridient: TotalNutrients): String? {
        return if (ingridient == null) {
            null
        } else {
            val jsonAdapter = moshi.adapter(TotalNutrients::class.java)
            val json: String = jsonAdapter.toJson(ingridient)
            return json
        }
    }
}




