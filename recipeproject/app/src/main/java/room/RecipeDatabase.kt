package room


import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import model.foodrecipes.totalnutrients.Hit
import androidx.room.Room


@Database(entities = arrayOf(Hit::class), version = 1)
@TypeConverters(ConverterRecipe::class)
abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao



    companion object {

        @Volatile private var INSTANCE: RecipeDatabase? = null

        fun getInstance(context: Context): RecipeDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        RecipeDatabase::class.java,"recipe-database" )
                        .build()
    }
}

